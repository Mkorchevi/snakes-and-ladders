﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoxelKata
{
    public class Ladders : Obstacle
    {
       
        //constructor
        public Ladders(string Name, int Destiny) : base(Name, Destiny)
        {
          
        }

        //método
        public override void Message()
        {
            Console.WriteLine("Great! Has landed on the ladder box, you move up to {0}.. ", Destiny);
        }
        

    }
}
