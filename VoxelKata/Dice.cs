﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoxelKata
{
    public class Dice
    {
        public int Roll()
        {
            Random dice = new Random();
            int number = dice.Next(1, 7);
            return number;
        }
    }
}
