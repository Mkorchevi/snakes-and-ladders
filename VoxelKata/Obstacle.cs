﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoxelKata
{
    public class Obstacle
    {
        //propiedades 
        public string Name { get; set; }
        public int Destiny { get; set; }

        //constructor
        public Obstacle(string Name, int Destiny) 
        {
            this.Name = Name;
            this.Destiny = Destiny;

        }

        //método
        public virtual void Message()
        {
            throw new NotImplementedException();
        }
    }
}
