﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VoxelKata
{
    public class Snake: Obstacle
    {
        //constructor
        public Snake(string Name, int Destiny) : base(Name, Destiny)
        {
           
        }

        //método
        public override void Message()
        {
            Console.WriteLine("Opps! Sorry, you have fallen into the snake box, now you back off to {0}.. ", Destiny);
        }
      

    }
}
